#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>

/*******************************************************************************/

#define PIPE_NAME               ( "pipe1" )

#define LIMIT_NUMBER            ( 100 )

/*******************************************************************************/

int repetitions_count = 0;

/*******************************************************************************/

void child_process_fn(int fd_pipe) {
    pid_t pid;
    
    // Get child PID
    pid = getpid();
    printf("child_process_fn (pid = %d) begins.\n", pid);

    // Perform a given number of repetitions
    for (uint8_t i = 0; i < repetitions_count; i++) {
        uint8_t rnd_number1, rnd_number2;
        char rnd_op;
        int ret_val; 

        // Read first operand
        ret_val = read(fd_pipe, &rnd_number1, 1);
        if (ret_val =! 1) {
            perror("child_process_fn: error reading from pipe.\n");
            exit(EXIT_FAILURE);
        }

        // Read operation code
        ret_val = read(fd_pipe, &rnd_op, 1);
        if (ret_val =! 1) {
            perror("child_process_fn: error reading from pipe.\n");
            exit(EXIT_FAILURE);
        }

        // Read second operand
        ret_val = read(fd_pipe, &rnd_number2, 1);
        if (ret_val =! 1) {
            perror("child_process_fn: error reading from pipe.\n");
            exit(EXIT_FAILURE);
        }

        // Perform operation
        int result;
        switch (rnd_op) {
            case '+':
                result = rnd_number1 + rnd_number2;
                break;
            case '-':
                result = rnd_number1 - rnd_number2;
                break;
            case '*':
                result = rnd_number1 * rnd_number2;
                break;
            case '/':
                result = rnd_number1 / rnd_number2;
                break;
            default:
                perror("child_process_fn: error operating.\n");
                exit(EXIT_FAILURE);
        }

        printf("child_process_fn (pid = %d): %d %c %d = %d\n", pid, rnd_number1, rnd_op, rnd_number2, result);
    }

    printf("child_process_fn (pid = %d) ends.\n", pid);
}

/*******************************************************************************/

void parent_process_fn(int fd_pipe) {
    char operations[] = {'+', '-', '*', '/'};

    pid_t pid;
    int status = 0;

    // Get parent PID
    pid = getpid();
    printf("parent_process_fn (pid = %d) begins.\n", pid);

    // Perform a given number of repetitions
    for (int i = 0; i < repetitions_count; i++) {
        uint8_t rnd_number1, rnd_number2, rnd_op;

        printf("parent_process_fn (pid = %d): iteration %d.\n", pid, i);
        
        // Generate a random operation
        rnd_number1 = random() % LIMIT_NUMBER;
        rnd_op      = operations[random() % (sizeof(operations)/ sizeof(operations[0]))];
        rnd_number2 = random() % LIMIT_NUMBER;

        printf("parent_process_fn (pid = %d): %d %c %d = ?\n", pid, rnd_number1, rnd_op, rnd_number2);

        // Write first operand, operation and second operand to pipe
        write(fd_pipe, &rnd_number1, 1);
        write(fd_pipe, &rnd_op, 1);
        write(fd_pipe, &rnd_number2, 1);

        // Sleep for 1 second
        sleep(1);
    }

    printf("parent_process_fn (pid=%d) ends.\n", pid);
}

/*******************************************************************************/

int main(int argc, char** argv) {
    pid_t pid;
    int ret_val;
    
    char* pipe = PIPE_NAME;
    int pipe_fd;

    // Validate number of parameters
    if (argc < 2) {
        perror("main: wrong number of parameters.\n");
        exit(EXIT_FAILURE);
    }

    // Convert string to integer
    repetitions_count = atoi(argv[1]);

    // Get time of day
    struct timeval tv;
    gettimeofday(&tv, NULL);

    // Seed the random number generator
    srand(tv.tv_sec);

    // Create pipe
    ret_val = access(pipe, F_OK);
    if (ret_val != 0) { // Does not exist
        // Create it
        ret_val = mkfifo(pipe, 0666);
        if (ret_val < 0) {
            perror("main: error creating pipe.\n");
            exit(EXIT_FAILURE);
        }
        printf("main: created pipe.\n");
    } else {
        printf("main: pipe exists, no need to create it.\n");
    }

    // Open pipe
    pipe_fd = open(pipe, O_RDWR);
    if (pipe_fd < 0) {
        perror("main: error opening pipe.\n");
        exit(EXIT_FAILURE);
    } else {
        printf("main: open pipe for read/write.\n");
    }
    
    // Fork to create a child process
    pid = fork();

    // Error creating the child process
    if (pid < 0) {
        perror("main: error in fork.");
        exit(EXIT_FAILURE);
    }
    
    // Child process
    else if (pid == 0) {
        child_process_fn(pipe_fd);
    } 
    
    // Parent process
    else {
        parent_process_fn(pipe_fd);
    }

    // Close pipe
    close(pipe_fd);

    // Unlink pipes
    unlink(pipe);

    exit(EXIT_SUCCESS);
}
