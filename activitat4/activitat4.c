#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <arpa/inet.h>
#include <netinet/in.h>

/*******************************************************************************/

#define QOTD_TCP_PORT           ( 17 ) 
#define QOTD_MAX_MESSAGES       ( 1024 )
#define QOTD_BUFFER_SIZE        ( 512 )

#define PIPE_FD_COUNT           ( 2 )
#define PIPE_BUFFER_SIZE        ( 512 )

#define PIPE_FD_READ            ( 0 )
#define PIPE_FD_WRITE           ( 1 )

/*******************************************************************************/

// Define quotd_msg as a data type that is a char array of size QOTD_BUFFER_SIZE
typedef char (quotd_msg_t)[QOTD_BUFFER_SIZE];

// Define quotd_buffer_t as a data type that is a quotd_msg_t array of size QOTD_MAX_MESSAGES
typedef quotd_msg_t (quotd_buffer_t)[QOTD_MAX_MESSAGES];

/*******************************************************************************/

static volatile bool finish = false;

/*******************************************************************************/

void sigint_handler(int dummy) {
    finish = true;
}

/*******************************************************************************/

bool read_quotd_file(char* file_name, quotd_buffer_t* quotd_buffer_ptr, int* qotd_messages) { 
    // Open file in read mode
    FILE * fp;
    fp = fopen(file_name, "r");
    if (fp == NULL) {
        return false;
    }

    // Pointer to the current QOTD message
    char * message_ptr;
    int message_counter = 0;

    // Point to the first QOTD message
    message_ptr = (*quotd_buffer_ptr)[message_counter];

    // Iterate until finished
    bool finished = false;
    do {
        char * line = NULL;
        size_t line_length = 0; 
        ssize_t read;

        // Read a line from the file
        // This will allocate memory using malloc
        read = getline(&line, &line_length, fp);

        // Check if it is the end of the file
        if (read < 0) {
            // Free the memory
            free(line);

            // Declare as finished
            finished = true;

            break;
        }

        // Check if this is the end of a QOTD
        if (line[0] == '%') {
            // Update the number of QOTD messages
            message_counter += 1;
            // Update pointer to the QOTD message
            message_ptr = (*quotd_buffer_ptr)[message_counter];
        } else {
            // Copy the line to the QOTD message
            // Make sure that we do not overflow by limiting to QOTD_BUFFER_SIZE
            // If the message is longer it will be cut
            strncpy(message_ptr, line, QOTD_BUFFER_SIZE);
            
            // Update the message pointer to keep writing at the end of it
            // in the next iteration of the loop
            message_ptr += read;
        }

        // Free the memory allocated by getline
        free(line);
    } while (!finished);
    
    // Close the file descriptor
    fclose(fp);

    // Update the number of quotes that have been read
    *qotd_messages = message_counter;

    return true;
}

/*******************************************************************************/

void child_process_fn(char* file_name, int pipe_read, int pipe_write) {
    // Buffers to read and write to/from the pipe
    char buffer_read[PIPE_BUFFER_SIZE];
    char buffer_write[PIPE_BUFFER_SIZE];
    
    int ret_val;

    // Allocate memory to QOTD messages
    quotd_buffer_t qotd_buffer;
    int qotd_messages = 0;
    
    // Clean the read/write buffers
    memset(buffer_read, 0, PIPE_BUFFER_SIZE);
    memset(buffer_write, 0, PIPE_BUFFER_SIZE);

    // Get child process
    pid_t pid;
    pid = getpid();

    printf("child_process_fn (pid=%d): begins.\n", pid);

    // Open and read the QOTD file to a memory structure
    bool status = read_quotd_file(file_name, &qotd_buffer, &qotd_messages);
    if (!status) {
        perror("child_process_fn: error opening or parsing the QOTD file.\n");
        exit(EXIT_FAILURE);
    }

    do {
        // Read request from the pipe
        ret_val = read(pipe_read, buffer_read, PIPE_BUFFER_SIZE);
        
        // Error reading from the pipe
        if (ret_val < 0) {
            perror("child_process_fn: error reading from the pipe.\n");
            exit(EXIT_FAILURE);
        } 
        
        // Nothing has been read from the pipe, move on!
        else if (ret_val == 0) {    
        }
        
        // Data read from the socket, process it
        else {
            int rnd_number, qotd_position; 
            char * qotd_message;
            int qotd_length;

            // Convert number string to integer
            rnd_number = atoi(buffer_read);
            
            // Convert random number to QOTD message position
            qotd_position = rnd_number % qotd_messages;

            printf("child_process_fn (pid=%d): read from the pipe (rnd_number=%d, qotd_position=%d).\n", pid, rnd_number, qotd_position);

            // Get pointer to the QTOD message and length
            qotd_message = qotd_buffer[qotd_position];
            qotd_length = strlen(qotd_message);

            // Write QOTD message to the pipe
            write(pipe_write, qotd_message, qotd_length);

            printf("child_process_fn (pid=%d): write QOTD message to the pipe (length=%d).\n", pid, qotd_length);
        }
    } while(!finish);
}

/*******************************************************************************/

void parent_process_fn(int pipe_read, int pipe_write) {   
    // Data structures and variables for the socket
    struct sockaddr_in server_addr, client_addr;
    int server_sock_fd, client_sock_fd;
    
    // Buffers to read and write to/from the pipe
    char buffer_read[PIPE_BUFFER_SIZE];
    char buffer_write[PIPE_BUFFER_SIZE];

    int ret_val;

    // Get the parent PID
    pid_t pid;
    pid = getpid();

    printf("parent_process_fn (pid=%d): begins.\n", pid);

    // Get time of day
    struct timeval tv;
    gettimeofday(&tv, NULL);

    // Seed the random number generator
    srand(tv.tv_sec);

    // Create a TCP socket
    server_sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_sock_fd < 0) {
        printf("parent_process_fn: error creating socket.\n");
        exit(EXIT_FAILURE);
    }
  
    // Initialize the servaddr structure
    // Allow connections from INADDR_ANY to the QTOD TCP port (17)
    bzero(&server_addr, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(QOTD_TCP_PORT);
  
    // Bind the socket to the servaddr structure
    ret_val = bind(server_sock_fd, (struct sockaddr *) &server_addr, sizeof(server_addr));
    if (ret_val < 0) {
        perror("parent_process_fn: error in socket bind.\n");
        exit(EXIT_FAILURE);
    }

    // Now server is ready to listen
    ret_val = listen(server_sock_fd, 1);
    if (ret_val < 0) {
        perror("parent_process_fn: error in socket listen.\n");
        exit(EXIT_FAILURE);
    }

    // Repeat until finish
    while (!finish) {   
        int buffer_length;
        int rnd_number;
        int length;

        // Clear the read/write buffers
        memset(buffer_read, 0, PIPE_BUFFER_SIZE);
        memset(buffer_write, 0, PIPE_BUFFER_SIZE);

        // Accept the connection from the client and return new file descriptor
        client_sock_fd = accept(server_sock_fd, (struct sockaddr *) &client_addr, &length);
        if (client_sock_fd < 0) {
            perror("parent_process_fn: error in socket accept.\n");
            exit(EXIT_FAILURE);
        }

        // Generate a random number
        rnd_number = random();

        // Convert the random number (int) to a string and write it to the buffer
        sprintf(buffer_write, "%d", rnd_number);
        buffer_length = strlen(buffer_write);

        // Write the buffer with the random number to the pipe
        ret_val = write(pipe_write, buffer_write, length);
        if (ret_val < 0) {
            perror("parent_process_fn: error in pipe write.\n");
            exit(EXIT_FAILURE);
        }
        else {
            printf("parent_process_fn (pid=%d): writes to pipe (rnd_number=%d).\n", pid, rnd_number);

            // Read QOTD message from the pipe
            ret_val = read(pipe_read, buffer_read, PIPE_BUFFER_SIZE);

            // Error reading from the socket
            if (ret_val < 0) {
                perror("parent_process_fn: error reading from pipe.\n");
                exit(EXIT_FAILURE);
            }
            
            // Nothing read from socket, move on!
            else if (ret_val == 0) {
            }
            
            // Data read from the socket, process it
            else {
                printf("parent_process_fn (pid=%d): read from the pipe (bytes=%d).\n", pid, ret_val);

                // Write the buffer to the client
                ret_val = write(client_sock_fd, buffer_read, ret_val);
                if (ret_val < 0) {
                    perror("parent_process_fn: error writing to socket.\n");
                    exit(EXIT_FAILURE);
                }

                printf("parent_process_fn (pid=%d) write to socket (bytes=%d).\n", pid, ret_val);

                // Close the client socket
                close(client_sock_fd);
            }
        }
    }
  
    // After chatting close the socket
    close(server_sock_fd); 

    printf("parent_process_fn (pid=%d): finish.\n", pid);
}

int main(int argc, char** argv) {
    pid_t pid;
    int pipe1_fd[PIPE_FD_COUNT];
    int pipe2_fd[PIPE_FD_COUNT];
    int ret_val;

    // Check input parameters
    if (argc != 2) {
        perror("Error! Wrong number of parameters.\n");
        exit(1);
    }

    // Create pipe1
    ret_val = pipe(pipe1_fd);
    if (ret_val != 0) {
        perror("main: error creating pipe1.\n");
        exit(EXIT_FAILURE);
    }

    // Create pipe2
    ret_val = pipe(pipe2_fd);
    if (ret_val != 0) {
        perror("main: error creating pipe2.\n");
        exit(EXIT_FAILURE);
    }
    
    // Fork the process to create a child
    pid = fork();

    // Error in the fork process
    if (pid < 0) {
        perror("main: error in fork.\n");
        exit(EXIT_FAILURE);
    }
    
    // Child process runs, write to pipe1 and read from pipe2
    else if (pid == 0) {
        // Close unused file descriptors
        close(pipe1_fd[PIPE_FD_READ]);
        close(pipe2_fd[PIPE_FD_WRITE]);

        // Run the child process function the deals with the QOTD messages
        child_process_fn(argv[1], pipe2_fd[PIPE_FD_READ], pipe1_fd[PIPE_FD_WRITE]);

        // Close remaining file descriptors
        close(pipe1_fd[PIPE_FD_WRITE]);
        close(pipe2_fd[PIPE_FD_READ]);
    }
    
    // Parent process runs, write to pipe2 and read from pipe1
    else {
        // Close unused file descriptors
        close(pipe1_fd[PIPE_FD_WRITE]);
        close(pipe2_fd[PIPE_FD_READ]);

        // Run the parent process function that deals with the socket
        parent_process_fn(pipe1_fd[PIPE_FD_READ], pipe2_fd[PIPE_FD_WRITE]);

        // Close remaining file descriptors
        close(pipe1_fd[PIPE_FD_READ]);
        close(pipe2_fd[PIPE_FD_WRITE]);
    }

    exit(EXIT_SUCCESS);
}
