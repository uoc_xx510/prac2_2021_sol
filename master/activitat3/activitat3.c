#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <sched.h>

#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <pthread.h>

/*******************************************************************************/

typedef struct {
    uint32_t * data_ptr;
    pthread_mutex_t * mutex1;
    pthread_mutex_t * mutex2;
} thread_data_t;

/*******************************************************************************/

void* child_process_fn(void * data_ptr) {
    bool is_finished = false;

    thread_data_t* data = (thread_data_t *) data_ptr;

    printf("thread2 begins, %d.\n", *data->data_ptr);  

    do  {
        // Wait semaphore1
        if (pthread_mutex_lock(data->mutex2) == 0) {

            // Finish or keep decrementing
            if ((*data->data_ptr) == 0) {
                is_finished = true;
            } else {
                (*data->data_ptr)--;
                printf("thread2 bounce %d.\n", (*data->data_ptr));
            }

            // Give semaphore2
            pthread_mutex_unlock(data->mutex1);
            
        } 
    } while (!is_finished);

    printf("thread2 ends.\n");
}

/*******************************************************************************/

void* parent_process_fn(void * data_ptr) {
    bool is_finished = false;
    
    thread_data_t* data = (thread_data_t *) data_ptr;

    printf("thread1 begins, %d.\n", *data->data_ptr);  
    
    do  {
        // Wait semaphore1
        if (pthread_mutex_lock(data->mutex1) == 0) {
            // Finish or keep decrementing
            if ((*data->data_ptr) == 0) {
                is_finished = true;
            } else {
                (*data->data_ptr)--;
                printf("thread1 bounce %d.\n", (*data->data_ptr));
            }

            // Give semaphore2
            pthread_mutex_unlock(data->mutex2);
        } 
    } while (!is_finished);

    printf("thread1 ends.\n");
}

/*******************************************************************************/

int main (int argc, char** argv) {
    pthread_mutex_t mutex1, mutex2;
    pthread_t t1, t2;

    thread_data_t thread_data;

    int ret_val;

    // Get time of day
    struct timeval tv;
    gettimeofday(&tv, NULL);

    // Seed the random number generator
    srand(tv.tv_sec);

    // Generate random number
    uint32_t bounces = random() % 20;

    printf("main: bouncing for %d times.\n", bounces);

    // Initialize mutex 1, by default it is unlocked
    ret_val = pthread_mutex_init(&mutex1, NULL);
    if (ret_val < 0) { 
        perror("main: Error creating mutex 1.");
        exit(EXIT_FAILURE);
    }

    // Initialize mutex 2, by default it is unlocked
    ret_val = pthread_mutex_init(&mutex2, NULL);
    if (ret_val < 0) { 
        perror("main: Error creating mutex 2.");
        exit(EXIT_FAILURE);
    }
    
    // Lock mutex2 to ensure parent starts
    pthread_mutex_lock(&mutex2);

    // Initialize thread_data structure
    thread_data.data_ptr = &bounces;
    thread_data.mutex1   = &mutex1;
    thread_data.mutex2   = &mutex2;

    // Create thread 1
    ret_val = pthread_create(&t1, NULL, &parent_process_fn, (void *) &thread_data);
    if (ret_val < 0) {
        perror("main: Error creating thread 1.");
        exit(EXIT_FAILURE);
    }

    // Create thread 2
    ret_val = pthread_create(&t2, NULL, &child_process_fn, (void *) &thread_data);
    if (ret_val < 0) {
        perror("main: Error creating thread 2.");
        exit(EXIT_FAILURE);
    }

    // Wait t1 and t2 to finish
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);

    // Destroy the mutex
    pthread_mutex_destroy(&mutex1);
    pthread_mutex_destroy(&mutex2);

    // Exit with success
    exit(EXIT_SUCCESS);
}
