#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <sched.h>

#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <sys/shm.h>
#include <sys/mman.h>
#include <semaphore.h>

/*******************************************************************************/

#define SEM_NAME1                    ( "/activitat4_sem1" )
#define SEM_NAME2                    ( "/activitat4_sem2" )

#define SHM_NAME                    ( "/activitat4_shm" )
#define SHM_LENGTH                  ( 4  )

/*******************************************************************************/

void child_process_fn(uint8_t* mem_ptr, sem_t* sem_ptr1, sem_t* sem_ptr2) {
    bool is_finished = false;
    uint32_t * shared_value;
    pid_t pid;
    
    // Get child PID
    pid = getpid();
    printf("child_process_fn (pid = %d) begins.\n", pid);

    // Point to shared memory as uint32_t
    shared_value = (uint32_t *) mem_ptr;

    do {
        // Wait semaphore2
        if (sem_wait(sem_ptr2) == 0) {
            // Finish or keep decrementing
            if (*shared_value == 0) {
                is_finished = true;
            } else {
                (*shared_value)--;
                printf("child_process_fn (pid = %d) bounce %d.\n", pid, *shared_value);  
            }

            // Post semaphore1
            sem_post(sem_ptr1);
        }
    } while(!is_finished);

    printf("child_process_fn (pid = %d) ends.\n", pid);
}

/*******************************************************************************/

void parent_process_fn(uint8_t* mem_ptr, sem_t* sem_ptr1, sem_t* sem_ptr2) {
    bool is_finished = false;
    pid_t pid;
    
    uint32_t current_value;
    uint32_t * shared_value;

    // Get parent PID
    pid = getpid();

    // Point to shared memory as uint32_t
    shared_value = (uint32_t *) mem_ptr;

    printf("parent_process_fn (pid = %d) begins.\n", pid);  
    
    do  {
        // Wait semaphore2
        if (sem_wait(sem_ptr1) == 0) {
            // Finish or keep decrementing
            if (*shared_value == 0) {
                is_finished = true;
            } else {
                (*shared_value)--;
                printf("parent_process_fn (pid = %d) bounce %d.\n", pid, *shared_value);
            }

            // Give semaphore2
            sem_post(sem_ptr2);
        }
    } while (!is_finished);

    printf("parent_process_fn (pid = %d) ends.\n", pid);
}

/*******************************************************************************/

int main (int argc, char** argv) {
    pid_t pid;
    int ret_val;

    // Get time of day
    struct timeval tv;
    gettimeofday(&tv, NULL);

    // Seed the random number generator
    srand(tv.tv_sec);

    // Generate random number
    uint32_t bounces = random() % 20;

    printf("main: bouncing for %d times.\n", bounces);

    // Create a shared memory object
    int fd = shm_open(SHM_NAME, O_RDWR | O_CREAT, 0644);
    if (fd < 0) {
        perror("main: Error creating shared memory object.");
        exit(EXIT_FAILURE);
    } 

    // Trucate file to specific size
    ftruncate(fd, SHM_LENGTH);

    // Create memory mapping
    uint8_t* mem_ptr = mmap(NULL, SHM_LENGTH, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (mem_ptr == MAP_FAILED) {
        perror("main: Error creating memory mapping.");
        exit(EXIT_FAILURE);
    }

    *((uint32_t *) mem_ptr) = bounces;

    // Create semaphore
    sem_t* sem_ptr1 = sem_open(SEM_NAME1, O_CREAT, 0644, 0);
    if (sem_ptr1 == (void*) -1) {
        perror("main: Error creating semaphore1.");
        exit(EXIT_FAILURE);
    }

    // Create semaphore
    sem_t* sem_ptr2 = sem_open(SEM_NAME2, O_CREAT, 0644, 0);
    if (sem_ptr2 == (void*) -1) {
        perror("main: Error creating semaphore2.");
        exit(EXIT_FAILURE);
    }

    // Make sure semaphores are initialized
    sem_init(sem_ptr1, 1, 1);
    sem_init(sem_ptr2, 1, 0);
 
    // Fork to create a child process
    pid = fork();

    // Error creating the child process
    if (pid < 0) {
        perror("main: error in fork.");
        exit(EXIT_FAILURE);
    }
    
    // Child process
    else if (pid == 0) {
        child_process_fn(mem_ptr, sem_ptr1, sem_ptr2);
    } 
    
    // Parent process
    else {
        parent_process_fn(mem_ptr, sem_ptr1, sem_ptr2);
    }

    // Unmap shared memory
    munmap(mem_ptr, SHM_LENGTH);

    // Close semaphores
    sem_close(sem_ptr1);
    sem_close(sem_ptr2);

    // Close file
    close(fd);
    
    // Unlink shared memory
    unlink(SHM_NAME);

    // Exit with success
    exit(EXIT_SUCCESS);
}
